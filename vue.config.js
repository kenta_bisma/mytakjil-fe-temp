const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  css: {
    loaderOptions: {
      sass: {
        additionalData: `@import "@/assets/sass/_shared.sass"`
      }
    }
  },
  devServer: {
    allowedHosts: "fe-service-mytakjil.herokuapp.com"
  }
})
