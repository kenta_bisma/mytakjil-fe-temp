import { createRouter, createWebHistory, NavigationGuardNext, RouteLocationNormalized } from 'vue-router'
import LoginView from '@/views/LoginView.vue'
import MenuView from '@/views/MenuView.vue'
import OrderView from '@/views/OrderView.vue'
import ClientOrderView from '@/views/ClientOrderView.vue'
import store from '@/store'
const routes = [
  {
    path: '/',
    name: 'home',
    component: MenuView,
    meta: {
      requireLogin: true
    }
  },
  {
    path: '/login',
    name: 'login',
    component: LoginView,
    meta: {
      requireLogin: false
    }
  },
  {
    path: '/menu',
    name: 'menu',
    component: MenuView,
    meta: {
      requireLogin: true,
      requireCustomer: true
    }
  },
  {
    path: '/your-order',
    name: 'your-order',
    component: ClientOrderView,
    meta: {
      requireLogin: true,
      requireCustomer: true
    }
  },
  {
    path: '/order',
    name: 'order',
    component: OrderView,
    meta: {
      requireLogin: true,
      requireStaff: true
    }
  },
  // {
  //   path: '/about',
  //   name: 'about',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  // }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export function beforeEach(to: RouteLocationNormalized, from: RouteLocationNormalized, next: NavigationGuardNext) {
  if (to.matched.some(record => record.meta.requireLogin) && !store.state.isAuthenticated) {
    next({ name: 'login' });
  } else if (to.matched.some(record => record.meta.secure) && store.state.isAuthenticated && store.state.isAdmin) {
    next({ name: 'order' })
  } else if (to.matched.some(record => record.meta.secure) && store.state.isAuthenticated && !store.state.isAdmin) {
    next({ name: 'menu' })
  } else if (to.matched.some(record => record.meta.requireJuru) && store.state.isAuthenticated && store.state.isAdmin) {
    next({ name: 'order' })
  } else if (to.matched.some(record => record.meta.requireAdmin) && store.state.isAuthenticated && !store.state.isAdmin) {
    next({ name: 'menu' })
  } else {
    next()
  }
}

router.beforeEach((to, from, next) => beforeEach(to, from, next))

export default router
