import { createStore } from 'vuex'

export default createStore({
  state: {
    isAuthenticated: false,
    token: '',
    refresh: '',
    isAdmin: false,
  },
  getters: {
  },
  mutations: {
    initializeStore(state) {
      if (localStorage.getItem('token')) {
        state.token = localStorage.getItem('token')!
        state.refresh = localStorage.getItem('refresh')!
        state.isAuthenticated = true

        // var data = jwtDecode(state.token)
        // state.isAdmin = data.is_admin
        // state.email = data.email
      } else {
        state.token = ''
        state.isAuthenticated = false
      }
    },
    setToken(state, token) {
      state.token = token
      state.isAuthenticated = true
      localStorage.setItem("token", token);
    },
    setRefresh(state, refresh) {
      state.refresh = refresh
      state.isAuthenticated = true
      localStorage.setItem("refresh", refresh);
    },
    setIdentity(state, token) {
      // var data = jwtDecode(token)
      // state.isAdmin = data.is_admin
    },
    removeToken(state) {
      state.token = ''
      state.isAuthenticated = false
      localStorage.removeItem('token')

      state.isAdmin = false
    },
    removeRefresh(state) {
      state.refresh = ''
      state.isAuthenticated = false
      localStorage.removeItem('refresh')
    }
  },
  actions: {
  },
  modules: {
  }
})
