export const NAIVE_THEME_OVERRIDES = {
    "common": {
        "primaryColor": "#68bd41",
        "primaryColorHover": "#68bd41",
        "primaryColorPressed": "#68bd41",
        "primaryColorSuppl": "#f02438",
      },
}